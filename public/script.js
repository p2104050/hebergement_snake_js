var btn = document.getElementById("btn");
btn.addEventListener("click", start);

var ecran1 = document.getElementById("ecran1");

var canvas = document.getElementById("mycanvas");
var ctx = canvas.getContext('2d');

var afficher_score = document.getElementById("score");

var radios = document.getElementsByName("difficulte");

var delay;
var walls;
var score;
var food;
var direction;
var world;
var snake;
var jeu;
var url = "difficulte.json";

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function dessiner() {
    //console.log("dessiner");

    ctx.strokeStyle = "black";

    // dessiner le jeu
    for (let i=0; i<world.length; i++){
        for (let j=0; j<world[i].length; j++){
            // grille vide
            ctx.strokeRect(i*20, j*20, i*20+20, j*20+20);

            if (world[i][j] === "empty") {
                ctx.fillStyle = "gray";
            }
            if (world[i][j] === "snake") {
                ctx.fillStyle = "green";
            }
            if (world[i][j] === "food") {
                ctx.fillStyle = "red";
            }
            if (world[i][j] === "wall") {
                ctx.fillStyle = "black";
            }
            ctx.fillRect(i*20, j*20, i*20+20, j*20+20);
        }
    }
}

function ajout_food() {
    //ajoute et affiche la nourritue
    let x = getRandomInt(25);
    let y = getRandomInt(25);
    while (world[x][y] !== "empty"){
        x = getRandomInt(25);
        y = getRandomInt(25);
    }
    food[0] = x;
    food[1] = y;

    //agrandit le serpent
    let end_snake = []
    if (snake[snake.length - 1][2] === "right") {
        end_snake[0] = snake[snake.length - 1][0]-1;
        end_snake[1] = snake[snake.length - 1][1];
        end_snake[2] = snake[snake.length - 1][2];
    }
    if (snake[snake.length - 1][2] === "left") {
        end_snake[0] = snake[snake.length - 1][0]+1;
        end_snake[1] = snake[snake.length - 1][1];
        end_snake[2] = snake[snake.length - 1][2];
    }
    if (snake[snake.length - 1][2] === "up") {
        end_snake[0] = snake[snake.length - 1][0];
        end_snake[1] = snake[snake.length - 1][1]+1;
        end_snake[2] = snake[snake.length - 1][2];
    }
    if (snake[snake.length - 1][2] === "down") {
        end_snake[0] = snake[snake.length - 1][0];
        end_snake[1] = snake[snake.length - 1][1]-1;
        end_snake[2] = snake[snake.length - 1][2];
    }
    snake.push(end_snake);
    score++;
}

function start() {
    console.log("main");

    ecran1.style.display = 'none';
    canvas.style.display = 'block';

    score = 0;
    direction = "right";

    for(let i = 0; i < radios.length; i++){
        if(radios[i].checked){
            fetch(url)
                .then(function(response) {
                    if (response.ok) {
                        return response.json(); // une promesse
                    } else {
                        throw ("Error " + response.status);
                    }
                })
                .then (function(data) {
                    // traitement des données reçues
                    delay = data.difficulte[i].delay;
                    walls = data.difficulte[i].walls;
                    food = data.difficulte[i].food;
                    snake = data.difficulte[i].snake;
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    }
    
    clearInterval(jeu);
    jeu = setInterval(step, delay);

    // récupérer les touche du clavier 
    document.addEventListener('keydown', function (event) {
        if (event.key === "ArrowUp") {
            console.log("up");
            if (direction !== "down") {direction = "up";}
        }
        if (event.key === "ArrowDown") {
            console.log("down");
            if (direction !== "up") {direction = "down";}
        }
        if (event.key === "ArrowLeft") {
            console.log("left");
            if (direction !== "right") {direction = "left";}
        }
        if (event.key === "ArrowRight") {
            console.log("right");
            if (direction !== "left") {direction = "right";}
        }
    })
}

function step(){
    //console.log("step");

    world = []
    for (let i=0; i<25; i++){
        world.push([]);
        for (let j=0; j<25; j++){
            world[i].push("empty");
        }
    }

    world[food[0]][food[1]] = "food";

    //ajoute les murs dans le world
    for (let i = 0; i < walls.length; i++){
        world[walls[i][0]][walls[i][1]] = "wall";
    }

    for (let i = snake.length-1; i>=0; i--){
        if (snake[i][2] === "right"){
            if ((world[snake[0][0]][snake[0][1]] === "food") && (i===0)){
                //ajouter food
                ajout_food();
            }
            snake[i][0]++;
            if (i === 0){snake[i][2] = direction;}
            else{snake[i][2] = snake[i-1][2];}
        }
        if (snake[i][2] === "left"){
            if ((world[snake[0][0]][snake[0][1]] === "food") && (i===0)){
                //ajouter food
                ajout_food();
            }
            snake[i][0]--;
            if (i === 0){snake[i][2] = direction;}
            else{snake[i][2] = snake[i-1][2];}
        }
        if (snake[i][2] === "up"){
            if ((world[snake[0][0]][snake[0][1]] === "food") && (i===0)){
                //ajouter food
                ajout_food();
            }
            snake[i][1]--;
            if (i === 0){snake[i][2] = direction;}
            else{snake[i][2] = snake[i-1][2];}
        }
        if (snake[i][2] === "down"){
            if ((world[snake[0][0]][snake[0][1]] === "food") && (i===0)){
                //ajouter food
                ajout_food();
            }
            snake[i][1]++;
            if (i === 0){snake[i][2] = direction;}
            else{snake[i][2] = snake[i-1][2];}
        }
    }

    //condition fin du jeu
    //collision bord du world
    let perdu = false;
    if ((snake[0][0] < 0) || (snake[0][1] < 0) || (snake[0][0] > 24) || (snake[0][1] > 24)) {
        perdu = true;
    }
    else{
        //collision avec serpent
        for (let i = 1; i < snake.length; i++){
            if ((snake[0][0] === snake[i][0]) && (snake[0][1] === snake[i][1])) {
                perdu = true;
            }
        }
        //collision avec wall
        for (let i = 1; i < walls.length; i++){
            if ((snake[0][0] === walls[i][0]) && (snake[0][1] === walls[i][1])) {
                perdu = true;
            }
        }
    }
    //arret du jeu si perdu
    if (perdu) {
        console.log("perdu");
        clearInterval(jeu);
        ecran1.style.display = 'block';
        afficher_score.textContent = "score : "+score;
        canvas.style.display = 'none';
    }
    else {
        //ajoute le serpent dans le world
        for (let i = 0; i < snake.length; i++){
            world[snake[i][0]][snake[i][1]] = "snake";
        }
    }

    dessiner()
}